const { merge } = require('webpack-merge')
const common = require('./common')
const ip = require('ip')
const path = require('path')
const webpack = require('webpack')

module.exports = merge(common, {
    mode: 'development',
    devtool: 'inline-source-map',
    devServer: {
        host: ip.address(),
        port: 3000,
        open: true,
        hot: true,
        static: {
            directory: path.join(__dirname, '../dist'),
        },
        historyApiFallback: true,
    },
    plugins: [
        new webpack.DefinePlugin({
            'process.env': `'dev'`
        })
    ]
})